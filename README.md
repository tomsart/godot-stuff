Godot tips and general gamedev resources
========================================

Just some things I've found useful while learning to use Godot!


- [Godot Tips](#godot-tips)
- [Godot Recipes](#godot-recipes)
- [Gamedev Resources](#gamedev-resources)
- [Godot for Unity Users](./GodotForUnityUsers.txt)

Godot Tips
----------
- Your first stop should be Calinou's huge list of Godot specific resources: https://github.com/Calinou/awesome-godot

- Learning VR for Godot:
  - Bastiaan Olij's https://github.com/GodotVR/godot_openvr
  - Bastiaan's VR Youtube tutorials: https://www.youtube.com/playlist?list=PLe63S5Eft1KYGqgkx9I70vfWmvvOZ7apF
  - https://docs.godotengine.org/en/3.1/tutorials/vr/vr_starter_tutorial.html

- Put an (empty) file called .gdignore in any directory you want Godot to ignore

- Get familiar with 3d file Import options. For instance, there are many ways to preserve materials on reimport,
  and many ways to divide the contents of the import to subscenes and independent meshes

- If working with kitbashes, in import settings, make sure "Storage" is set to instanced subscenes. This will create 
  a different scene for each top level object.

- Debug -> view collision areas will show RayCast lines, don't confuse them for your vr pointer object for several hours!!

- adjust `default_env.tres` right off the bat. This is used as the default sky and ambient light for any scene that doesn't have its own.

      Blender: filmic
      Tone mapping:
        exposure 1, white between 4 and 16, use filmic
      Ambient
        set maybe middle gray
        adjust sky contribution.. default is all the way up
        Adjust shadow after main light
      More thorough discussion here: https://www.youtube.com/watch?v=8kwnCxK8Vc8&feature=youtu.be

- Build from the command line: `godot --export "Windows Desktop" some_name`

- Enter editor from the command line when in some project's top directory: `godot -e`

- When importing objects into Godot, PUT EACH IN THEIR OWN DIRECTORY, else you get your materials all messed up if you are not very careful

- Merge from scene vs just drop in: You loose direct links to imported objects, but you have much more flexibility for reparenting, renaming, etc.
  
- About creating addons: https://docs.godotengine.org/en/3.1/tutorials/plugins/editor/making_plugins.html

- Learning editor scripts: 
  - https://docs.godotengine.org/en/3.2/tutorials/plugins/editor/
  - GamesFromScratch via GDQuest, basic "tool" based script on random objects: https://www.youtube.com/watch?v=XPs-HGzElTg
  - GDQuest via GamesFromScratch: https://www.youtube.com/watch?v=QHCdeBzdmlA

- When you model in Blender (or wherever) USE REASONABLE MEASUREMENTS!!!!
  So if something is supposed to be about 6 feet tall, model it that way,
  don't rely on scaling it later, it complicates everything, especially anything that relies on physics.

- Make sure scaling sgns equals out to positive, else lighting later on in Godot gets "reversed"

- Name your things meaningfully, not cube.023. Name your directories meaningfully too, while you're at it.

- When rigging in blender, START with mirror plane YZ, as you can only symmetrize by default with this plane. If you don't do this,
  it is very difficult to symmetrize after the fact. If you have an easy addon that can fix this, please let me know!!!!

- Automatic collider objects on import to Godot, done with special naming:

      \*-col: Creates mesh - static body - concave collision shape
      \*-colonly: Creates static body - concave collision shape
      Set display to wireframe in blender so you can still see things

- Quaternion visualization: https://www.youtube.com/watch?v=d4EgbgTm0Bg


Godot Recipes
----------------
- custom shaders with uniforms updated with code:

      $SomeMeshInstance.get_surface_material(0).set_shader_param("ws", ws)

- Vive controller mapping:
 
      controller.get_joystick_axis(which)
      controller.is_button_down(which)
        Axis 0 = Left/Right on the touchpad
        Axis 1 = Forward/Backward on the touchpad
        Axis 2 = Front trigger
        Button 1 = menu button
        Button 2 = side buttons
        Button 14 = press down on touchpad
        Button 15 = Front trigger (on/off version of Axis 2)

- git godot needs custom export templates. for linux:

      scons platform=x11 tools=no target=release bits=64
      scons platform=x11 tools=no target=release_debug bits=64
	  
  This makes files in `godot/bin/`.
  Export Templates are basically just the game engine without the editor environment, built for specific platforms

- How to pretend you are using an editor button, but you are really using an exported bool:

  Designed to run on property update in editor. NEEDS TO BE A SCRIPT ON AN OBJECT IN SCENE!
  ```
      tool
      extends Whatever
      export (bool) var trigger = false setget StuffToRun
      func StuffToRun(value):
          if Engine.editor_hint:
              #....do stuff
      func _draw()
      func _process():
          if Engine.editor_hint:
              print ("running within editor")
          else
              print ("running outside editor")
  ```
- Manually run currently active script. `file->run` YOU HAVE TO BE IN SCRIPT WINDOW (not 3d view)!!
  ```
      tool
      extends EditorScript
      func _run():
          var cube = MeshInstance.new()
          cube.set_name("Cube")
          cube.mesh = CubeMesh.new()
          get_scene().add_child(cube)
          cube.set_owner(get_scene())
  ```

Gamedev Resources
=================

Editing Tools
-------------
Base texture editing
- Krita
- Gimp
- Inkscape

PBR texture building
- Armorpaint: https://armorpaint.org/
- Texture Lab: https://njbrown.itch.io/texturelab
- Material Maker: https://github.com/RodZill4/material-maker
- Blender: https://blender.org

Modeling
- Blender

Linux Friendly Screen shots, videos:
 - Flameshot
 - SimpleScreenRecorder
 - Kdenlive

Shader utilities:
- 2d sdf generator from image: https://github.com/ConnyOnny/sdfgen.git
- ShaderED, shader editor and debugging: https://shadered.org/index.php
- For study, hard to beat https://www.shadertoy.com/


Images, Models, Sounds...
-------------------------

__General_assets__
- Open assets by Jeremy Bullock: https://www.youtube.com/watch?v=iFzpOjWmuYc
- OpenGameArt: https://opengameart.org
- Kenney: http://kenney.nl/assets 
- Itch.io: https://itch.io/game-assets
- Glitch: https://www.glitchthegame.com/public-domain-game-art/
- Game Art 2D: https://www.gameart2d.com/freebies.html 

__Sound_and_music__
- Incompetech: https://incompetech.com/music/royalty-free
- Free Sound: https://freesound.org/ 
- Sound Bible: http://soundbible.com/ 
- Free to Use Sounds: https://freetousesounds.com/
- Yellowstone: https://www.nps.gov/yell/learn/photosmultimedia/soundlibrary.htm

__Reference_images__
- Unsplash.com
- see also PureRef, free to use if you want, but closed source, but so clean and easy, I have to recommend

__3d_models__
- Smithsonian open assets library, tons of images and models: https://www.si.edu/openaccess
- Several CC0 good quality models: https://3dmodelhaven.com/
- Sketchfab has a number of cc0'd assets
- CC0 Models from Molopolka Museum in Poland on Sketchfab: `https://sketchfab.com/search?features=downloadable&q=malopolska+&sort_by=-relevance&type=models`
- Blendswap.com

